---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: model-gateway
  labels:
    app: model-gateway
    {{- include "ai-assist.labels" . | nindent 4 }}
    {{ include "common.tplvalues.render" (dict "value" .Values.modelGateway.labels "context" $) }}
spec:
  replicas: {{ .Values.modelGateway.hpa.minReplicas | int }}
  selector:
    matchLabels:
      app: model-gateway
      {{- include "ai-assist.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        app: model-gateway
        {{- include "ai-assist.selectorLabels" . | nindent 8 }}
        {{- include "ai-assist.labels" . | nindent 8 }}
        {{ include "common.tplvalues.render" (dict "value" .Values.modelGateway.labels "context" $) }}
    spec:
      # Allow these pods access to an IAM service account
      serviceAccountName: model-gateway-serviceaccount
      nodeSelector:
        # Only run on nodes where the GKE Metadata server is enabled
        # (the CPU node pool is configured to use GKE Metadata)
        iam.gke.io/gke-metadata-server-enabled: "true"
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: model-gateway
          image: {{ .Values.modelGateway.modelGateway.image.repository }}:{{ .Values.modelGateway.modelGateway.image.tag }}
          ports:
            - name: api-port
              containerPort: 5052
            - name: metrics-port
              containerPort: 8082
          volumeMounts:
            - name: third-party-ai-limited-access
              mountPath: "/etc/third-party-ai-limited-access"
              readOnly: true
          env:
            - name: FASTAPI_API_PORT
              value: {{ quote .Values.modelGateway.modelGateway.env.fastapiApiPort }}
            - name: GITLAB_URL
              value: {{ quote .Values.modelGateway.modelGateway.env.gitlabUrl }}
            - name: GITLAB_API_URL
              value: {{ quote .Values.modelGateway.modelGateway.env.gitlabApiUrl }}
            - name: CUSTOMER_PORTAL_BASE_URL
              value: {{ quote .Values.modelGateway.modelGateway.env.customerPortalBaseUrl }}
            # Enable the Google Cloud Profiler
            # See https://console.cloud.google.com/profiler?referrer=search&project=unreview-poc-390200e5
            # for profile output
            - name: GOOGLE_CLOUD_PROFILER
              value: {{ quote .Values.modelGateway.modelGateway.env.googleCloudProfiler }}

            # Feature flag limiting third-party access to certain ProjectIds on
            # GitLab.com
            - name: F_THIRD_PARTY_AI_LIMITED_ACCESS
              value: {{ quote .Values.modelGateway.modelGateway.flags.third_party_limited_ai_access }}
            - name: F_IS_THIRD_PARTY_AI_DEFAULT
              value: {{ quote .Values.modelGateway.modelGateway.flags.is_third_party_ai_default }}
            - name: F_THIRD_PARTY_ROLLOUT_PERCENTAGE
              value: {{ quote .Values.modelGateway.modelGateway.flags.third_party_rollout_percentage }}
            - name: PALM_TEXT_MODEL_NAME
              value: {{ quote .Values.modelGateway.modelGateway.env.vertex_code_model_name }}
          resources:
            requests:
              cpu: {{ quote .Values.modelGateway.modelGateway.resources.requests.cpu }}
              memory: {{ quote .Values.modelGateway.modelGateway.resources.requests.memory }}
            limits:
              cpu: {{ quote .Values.modelGateway.modelGateway.resources.limits.cpu }}
              memory: {{ quote .Values.modelGateway.modelGateway.resources.limits.memory }}
      volumes:
        - name: third-party-ai-limited-access
          secret:
            secretName: third-party-ai-limited-access

          # livenessProbe:
          #   httpGet:
          #     path: /monitoring/healthz
          #     port: api-port
          #   initialDelaySeconds: 10
          #   periodSeconds: 10
          #   timeoutSeconds: 1
          #   failureThreshold: 3
---
apiVersion: v1
kind: Service
metadata:
  name: model-k8s-gateway
  labels:
    app: model-gateway
    {{- include "ai-assist.labels" . | nindent 4 }}
    {{ include "common.tplvalues.render" (dict "value" .Values.modelK8SGateway.labels "context" $) }}
{{- if ne (include "common.tplvalues.render" (dict "value" .Values.modelK8SGateway.service.annotations "context" $)) "null" }}
  annotations:
    {{- include "common.tplvalues.render" (dict "value" .Values.modelK8SGateway.service.annotations "context" $) | nindent 4 }}
{{- end }}
spec:
  type: {{ .Values.modelK8SGateway.service.type }}
  ports:
    - name: api
      port: 8080
      targetPort: api-port
    - name: metrics
      port: 8081
      targetPort: metrics-port
  selector:
    app: model-gateway
    {{- include "ai-assist.selectorLabels" . | nindent 4 }}
---
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: model-gateway
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: model-gateway
  minReplicas: {{ .Values.modelGateway.hpa.minReplicas | int }}
  maxReplicas: {{ .Values.modelGateway.hpa.maxReplicas | int }}
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: {{ .Values.modelGateway.hpa.cpu.targetAverageUtilization | int }}
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: model-gateway-servicemonitor
  labels:
    release: prometheus
    {{- include "ai-assist.labels" . | nindent 4 }}
    type: monitoring
spec:
  endpoints:
    - interval: 30s
      port: metrics
  selector:
    matchLabels:
      app: model-gateway
      {{- include "ai-assist.selectorLabels" . | nindent 6 }}
