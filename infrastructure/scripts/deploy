#!/usr/bin/env python3

import argparse
import json
import logging
import os
import subprocess

logging.basicConfig(level=logging.DEBUG, format="%(asctime)s: %(levelname)s - %(message)s")

gcp_zone = 'us-central1-c'
gcp_project = 'unreview-poc-390200e5'
namespace = 'fauxpilot'

env_configs = {
    'gprd': {
        'kube_ctx': 'gke_unreview-poc-390200e5_us-central1-c_ai-assist',
        'cluster_name': 'ai-assist',
        'helmfile': 'helmfile.yaml',
        'resolve_deploys': True
    },
    'gstg': {
        'kube_ctx': 'gke_unreview-poc-390200e5_us-central1-c_ai-assist-test',
        'cluster_name': 'ai-assist-test',
        'helmfile': 'helmfile.yaml',
        'resolve_deploys': True
    },
    'runner': {
        'kube_ctx': 'gke_unreview-poc-390200e5_us-central1-c_ai-assist-test',
        'cluster_name': 'ai-assist-test',
        'helmfile': 'helmfile-runner.yaml',
        'resolve_deploys': False
    }
}

infra_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))

# Runs init
def init_command(opts):
    env_config = env_configs[opts.env]
    cluster_name = env_config["cluster_name"]

    cmd = f"gcloud container clusters get-credentials '{cluster_name}' --zone '{gcp_zone}' --project '{gcp_project}'"
    subprocess.run(cmd, check=True, shell=True)

# Get a helm command
def get_helmfile_cmd(opts, subcommand):
    env_config = env_configs[opts.env]
    helmfile = env_config["helmfile"]
    debug_flag = "--debug" if opts.debug else ""

    set_args = get_helm_deployment_values(opts.env, opts.deploy)
    return f"helmfile {subcommand} -f '{infra_dir}/{helmfile}' {debug_flag} --environment '{opts.env}' {' '.join(set_args)}"


# Runs helmfile diff...
def diff_command(opts):
    cmd = get_helmfile_cmd(opts, "diff")

    logging.info("Running: %s", cmd)
    subprocess.run(cmd, check=True, shell=True)


# Runs helmfile sync...
def sync_command(opts):
    if opts.dry_run:
        logging.warning("--no-dry-run not provided, running diff only")
        diff_command(opts)
        return

    cmd = get_helmfile_cmd(opts, "sync")

    logging.info("Running: %s", cmd)
    subprocess.run(cmd, check=True, shell=True)


# Extracts the current values for deployment tags to
# set them for the next deployment
def get_helm_deployment_values(env, deploy_args):
    env_config = env_configs[env]
    kube_ctx = env_config["kube_ctx"]
    resolve_deploys = env_config["resolve_deploys"]

    # Skip for runners
    if not resolve_deploys:
        return []

    cmd = f"helm get values -n '{namespace}' ai-assist -o json --all --kube-context '{kube_ctx}'"
    values_json = subprocess.check_output(cmd, shell=True)

    values = json.loads(values_json)

    if deploy_args is None:
        deploy_args = {}

    # Set the deployment tags from the previous deploy
    # or overrides via the `--deploy` argument...
    model_gateway_tag = deploy_args['model-gateway'] if 'model-gateway' in deploy_args else values['modelGateway']['modelGateway']['image']['tag']

    return ["--set", f"modelGateway.modelGateway.image.tag={model_gateway_tag}"]


# Used to parse name=value tags for updating deployments
class ParseDeployKwargs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())
        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key] = value


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', help='debug', action=argparse.BooleanOptionalAction, default=False)

    subparsers = parser.add_subparsers(title='subcommands',
                                    description='valid subcommands',
                                    help='additional help',
                                    required=True)

    init_parser = subparsers.add_parser('init')
    init_parser.add_argument('env', help='environment', choices=env_configs.keys())
    init_parser.set_defaults(func=init_command)

    diff_parser = subparsers.add_parser('diff')
    diff_parser.add_argument('env', help='environment', choices=env_configs.keys())
    diff_parser.add_argument('--deploy', nargs='*', action=ParseDeployKwargs, help="upgrade deployments service=tag")
    diff_parser.set_defaults(func=diff_command)

    sync_parser = subparsers.add_parser('sync', aliases=['upgrade', 'apply'])
    sync_parser.add_argument('--dry-run', action=argparse.BooleanOptionalAction, default=True)
    sync_parser.add_argument('env', help='environment', choices=env_configs.keys())
    sync_parser.add_argument('--deploy', nargs='*', action=ParseDeployKwargs, help="upgrade deployments service=tag")
    sync_parser.set_defaults(func=sync_command)

    return parser

parser = create_arg_parser()
opts = parser.parse_args()

opts.func(opts)
